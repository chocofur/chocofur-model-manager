import bpy

# updater ops import, all setup in this file
from . import addon_updater_ops

def cetegory_factory(category):
    def draw_func(self, context):
        category_prev = getattr(context.window_manager.chocofur_model_manager, category+"_previews")
        layout = self.layout

        ############## Category Panel ##############
    
        box = layout.box()
        ####### Drop Down Menu
        row = box.row()
        row.prop(context.scene.chocofur_model_manager, category+"_category", text="")
        ####### Previews
        row = box.row()
        row.scale_y = 1.5        
        row.template_icon_view(context.window_manager.chocofur_model_manager, category+"_previews", show_labels=True)
        ####### Model Name
        row = box.row()
        row.alignment = 'CENTER'
        row.scale_y = 0.5
        row.label(category_prev.split('.jpg')[0])
        ####### Add Button
        row = box.row()
        row.operator("chocofur.add", icon="ZOOMIN", text="Add {} Model".format(category)).object_type=category
        
    return type("CATEGORY_PT_chocofur_"+category, (bpy.types.Panel,), {
        "bl_label": category,
        "bl_space_type": 'VIEW_3D',
        "bl_region_type": 'TOOLS',
        "bl_category": "Chocofur Model Manager",
        "bl_options":{'DEFAULT_CLOSED'},
        "draw": draw_func,
    })
        
#################################################################
############################ Toolbar ############################
#################################################################
def options_panel_factory():
    def draw_func(self, context):
        layout = self.layout
        ############## Library Panel ##############
        box = layout.box()
        box.label(text="Library Path:")
        row = box.row()        
        row.operator("library.path", icon="ZOOMIN", text="Open Library Path")   
        
        ############## Import Options ##############
        box.label(text="Model Import Location:")
        row = box.row()        
        row.prop(context.window_manager.chocofur_model_manager, "append_location", expand=True)
        
        box.label(text="Model Import Method:")
        row = box.row()        
        row.prop(context.window_manager.chocofur_model_manager, "import_mode", expand=True)
        
        addon_updater_ops.update_notice_box_ui(self, context)
        
    return type("CATEGORY_PT_chocofur_OptionsPanel", (bpy.types.Panel,), {
        "bl_label": "Chocofur Model Manager",
        "bl_space_type": 'VIEW_3D',
        "bl_region_type": 'TOOLS',
        "bl_category": "Chocofur Model Manager",
        "draw": draw_func,
    })

############## Preferences ##############


# autoupdater preferences
from extensions_framework import util as efutil
def set_prop(propname, val):
    from . import bl_info
    efutil.write_config_value(bl_info['name'], 'defaults', propname, str(val))
    setattr(bpy, 'cmm_config_{}'.format(propname), val)
    return None

class ChocofurManagerPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__
   
    # addon updater preferences

    auto_check_update = bpy.props.BoolProperty(
        name="Auto-check for Update",
        description="If enabled, auto-check for updates using an interval",
        #default=False,
        get=lambda val: bpy.cmm_config_auto_check_update,
        set=lambda self, value: set_prop('auto_check_update', value),
        )
    
    updater_intrval_months = bpy.props.IntProperty(
        name='Months',
        description="Number of months between checking for updates",
        min=0,
        #default=0,
        get=lambda val: bpy.cmm_config_updater_intrval_months,
        set=lambda self, value: set_prop('updater_intrval_months', value),
        )
        
    updater_intrval_days = bpy.props.IntProperty(
        name='Days',
        description="Number of days between checking for updates",
        min=0,
        max=31,
        #default=7,
        get=lambda val: bpy.cmm_config_updater_intrval_days,
        set=lambda self, value: set_prop('updater_intrval_days', value),
        )
    
    updater_intrval_hours = bpy.props.IntProperty(
        name='Hours',
        description="Number of hours between checking for updates",
        min=0,
        max=23,
        #default=0,
        get=lambda val: bpy.cmm_config_updater_intrval_hours,
        set=lambda self, value: set_prop('updater_intrval_hours', value),
        )
    
    updater_intrval_minutes = bpy.props.IntProperty(
        name='Minutes',
        description="Number of minutes between checking for updates",
        min=0,
        max=59,
        #default=0,
        get=lambda val: bpy.cmm_config_updater_intrval_minutes,
        set=lambda self, value: set_prop('updater_intrval_minutes', value),
        )

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        row = layout.split(.75, align=False)
        row.prop(context.window_manager.chocofur_model_manager, "library_path")
        row.operator('chocofur.libpath_set_default')
        
        
        layout = self.layout
        # col = layout.column() # works best if a column, or even just self.layout
        mainrow = layout.row()
        col = mainrow.column()

        # updater draw function
        # could also pass in col as third arg
        addon_updater_ops.update_settings_ui(self, context)

        # Alternate draw function, which is more condensed and can be
        # placed within an existing draw function. Only contains:
        #   1) check for update/update now buttons
        #   2) toggle for auto-check (interval will be equal to what is set above)
        # addon_updater_ops.update_settings_ui_condensed(self, context, col)

        # Adding another column to help show the above condensed ui as one column
        # col = mainrow.column()
        # col.scale_y = 2
        # col.operator("wm.url_open","Open webpage ").url=addon_updater_ops.updater.website